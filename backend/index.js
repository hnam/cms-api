const functions = require("firebase-functions");
const app = require("express")();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const routes = require("./src/routes/index.route");
const errorHandler = require("./src/middlewares/errorHandler");
const joiErrorHandler = require("./src/middlewares/joiErrorHandler");
const dbConfig = require("./src/configs/database");

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
// load our API routes
app.use("/", routes);

// Joi Error Handler
app.use(joiErrorHandler);

// Error Handler
app.use(errorHandler.notFoundErrorHandler);
app.use(errorHandler.errorHandler);

//connect database .

mongoose
    .connect(dbConfig.db, {
        keepAlive: 1,
        useNewUrlParser: true,
        useCreateIndex: true
    })
    .catch(error => {
        console.log("ERROR:", error);
    });

exports.api = functions.region("asia-east2").https.onRequest(app);
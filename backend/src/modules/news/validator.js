const Joi = require("joi");

module.exports = {
    newsForm: {
        body: {
            name: Joi.string().required(),
            category: Joi.string().required(),
            imageURL: Joi.string().required(),
            description: Joi.string(),
            content: Joi.string(),
            tags: Joi.string()
        }
    }
};
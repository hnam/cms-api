const { bucket } = require("../../configs/FBAdmin");
const config = require("../../configs/FBConfig");
const HttpStatus = require("http-status-codes");

function processFile(req, res, files) {
    const path = require("path");
    const os = require("os");
    const fs = require("fs");
    const slugify = require("slugify");

    const Busboy = require("busboy");

    const busboy = new Busboy({
        headers: req.headers,
        limits: {
            fileSize: 2 * 1024 * 1024,
            files: files
        }
    });
    // INIT AVARIABLE
    let imageToBeUploaded = {}; // File upload {filepath,mimetype:}
    let imageFileName; //  File name
    const dataFiles = []; // Data array file
    let modulePath = null;
    busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
        //CHECK file
        console.log(fieldname);
        if (!file) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "File not found"
            });
        }
        if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: `Wrong file type submitted`
            });
        }

        //RENAME FILE
        const fileNames = filename.split(".");
        const cleanedFileName = slugify(fileNames[0], { remove: /[()]/g });
        const randomString = Math.round(Math.random() * 1000000000000).toString();
        const imageExtension = filename.split(".")[filename.split(".").length - 1];
        imageFileName = `${cleanedFileName}${randomString}.${imageExtension}`;
        //PATH
        const filepath = path.join(os.tmpdir(), imageFileName);
        file.pipe(fs.createWriteStream(filepath));
        imageToBeUploaded = { filepath, mimetype, imageFileName };
        dataFiles.push(imageToBeUploaded);
    });
    busboy.on("field", function(fieldname, val) {
        modulePath = val;
    });
    busboy.on("finish", () => {
        const folderUpload = "upload_images";
        const promises = dataFiles.map(item =>
            bucket
            .upload(item.filepath, {
                resumable: false,
                destination: `${folderUpload}/${modulePath}/${
            path.parse(item.filepath).base
          }`,
                metadata: {
                    metadata: {
                        contentType: item.mimetype
                    }
                }
            })
            .then(() => {
                return {
                    imageURL: encodeURI(
                        `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${folderUpload}%2F${modulePath}%2F${item.imageFileName}?alt=media`
                    )
                };
            })
            .catch(error => {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    success: false,
                    code: HttpStatus.BAD_REQUEST,
                    message: `Upload file error`,
                    error
                });
            })
        );

        return Promise.all(promises).then(result => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: `Upload file success`,
                data: result
            });
        });
    });

    busboy.end(req.rawBody);
}

exports.upload = (req, res) => {
    processFile(req, res, 1);
};

exports.uploadMulti = (req, res) => {
    processFile(req, res, 10);
};
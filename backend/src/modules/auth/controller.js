const config = require("../../configs/FBConfig");
const HttpStatus = require("http-status-codes");
const firebase = require("firebase");

firebase.initializeApp(config);

exports.logIn = (req, res) => {
  const { email, password } = req.body;

  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(data => {
      if (!data.user.emailVerified) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          success: false,
          code: HttpStatus.BAD_REQUEST,
          message: "Email is not verify"
        });
      }
      return data.user.getIdToken();
    })
    .then(token => {
      return res.status(HttpStatus.CREATED).json({
        success: true,
        code: HttpStatus.OK,
        message: `Login Success`,
        token
      });
    })
    .catch(error => {
      return res.status(HttpStatus.BAD_REQUEST).json({
        success: false,
        code: HttpStatus.BAD_REQUEST,
        message: "Something went wrong, please try again"
      });
    });
};
/**
 * CHANGE PASSWORD
 */
exports.changePassword = (req, res, next) => {
  const { email } = req.body;
  firebase
    .auth()
    .sendPasswordResetEmail(email)
    .then(() => {
      return res.status(HttpStatus.OK).json({
        success: true,
        code: HttpStatus.OK,
        message: "Send email success"
      });
    })
    .catch(error => {
      return res.status(HttpStatus.BAD_REQUEST).json({
        success: false,
        code: HttpStatus.BAD_REQUEST,
        message: "Something went wrong, please try again",
        error
      });
    });
};

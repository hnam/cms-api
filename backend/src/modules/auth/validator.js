const Joi = require("joi");

module.exports = {
  logIn: {
    body: {
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string()
        .regex(/^[a-zA-Z0-9]{3,30}$/)
        .min(6)
        .required()
    }
  },
  changePassword: {
    body: {
      email: Joi.string()
        .email()
        .required()
    }
  }
};

const mongoose = require("mongoose");
const HttpStatus = require("http-status-codes");
const Promise = require("bluebird");

const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
  address: {
    type: String
  },
  uid: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  },
  role: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

UserSchema.index({ email: 1, uid: 1 });

UserSchema.statics = {
  get(id) {
    return this.findById(id)
      .exec()
      .then(user => {
        if (user) {
          return user;
        }
        const err = new APIError("No such user exists!", httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

module.exports = mongoose.model("User", UserSchema);

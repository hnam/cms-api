const HttpStatus = require("http-status-codes");
const firebase = require("firebase");
const User = require("./model");
/**
 * LIST USER
 */
exports.list = function(req, res) {
    const { limit, skip } = req;
    User.list({ limit, skip })
        .then(data => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: true,
                code: HttpStatus.OK,
                message: "Load success",
                data
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Load error",
                error
            });
        });
};
/**
 * CREATE USER
 */
exports.create = function(req, res) {
    firebase
        .auth()
        .createUserWithEmailAndPassword(req.body.email, req.body.password)
        .then(async data => {
            let user = firebase.auth().currentUser;
            user
                .sendEmailVerification()
                .then(async() => {
                    const newUser = new User(req.body);
                    newUser.uid = data.user.uid;
                    try {
                        await newUser.save();
                        return res.status(HttpStatus.BAD_REQUEST).json({
                            success: true,
                            code: HttpStatus.OK,
                            message: "Load success",
                            data: newUser
                        });
                    } catch (error) {
                        throw "SAVE_FAILED";
                    }
                })
                .catch(error => {
                    throw "SAVE_FAILED";
                });
        })
        .catch(error => {
            if (error === "SAVE_FAILED") {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    success: false,
                    code: HttpStatus.BAD_REQUEST,
                    message: "Save have been problerm"
                });
            }
            if (error.code === "auth/email-already-in-use") {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    success: false,
                    code: HttpStatus.BAD_REQUEST,
                    message: "Email is already is use"
                });
            } else {
                return res.status(HttpStatus.BAD_REQUEST).json({
                    success: false,
                    code: HttpStatus.BAD_REQUEST,
                    message: "Something went wrong, please try again",
                    error
                });
            }
        });
};

/**
 * UPDATE USER
 */

exports.update = function(req, res) {
    const id = req.params.id;
    const updateObject = req.body;
    User.update({ _id: id }, { $set: updateObject })
        .exec()
        .then(() => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: "Update success"
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Update error. Please check again",
                error
            });
        });
};

/**
 * LOAD USER INFO
 */

exports.getInfo = function(req, res, next) {
    User.findById(req.params.id)
        .then(user => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: "Get info user Success",
                data: user
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Get info user error. Please check again",
                error
            });
        });
};
/**
 * REMOVE USER
 */
exports.remove = function(req, res, next) {
    const id = req.params.id;
    User.findByIdAndRemove(id)
        .exec()
        .then(() => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: "Delete user success"
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Delete user error. Please check again",
                error
            });
        });
};
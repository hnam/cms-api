const Joi = require("joi");

module.exports = {
  userForm: {
    body: {
      fullname: Joi.string().required(),
      address: Joi.string().required(),
      phone: Joi.string().required(),
      role: Joi.string().required(),
      email: Joi.string()
        .email()
        .required(),
      password: Joi.string()
        .regex(/^[a-zA-Z0-9]{3,30}$/)
        .min(6)
        .required(),
      confirmPassword: Joi.any()
        .valid(Joi.ref("password"))
        .required()
        .options({ language: { any: { allowOnly: "must match password" } } })
    }
  }
};

const HttpStatus = require("http-status-codes");
const NewsCategory = require("./model");

/**
 * LIST NEWS CATEGORY
 */

exports.list = function(req, res) {
    const { limit, skip } = req;
    NewsCategory.list({ limit, skip })
        .then(data => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: true,
                code: HttpStatus.OK,
                message: "Load success",
                data
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Load error",
                error
            });
        });
};

/**
 * CREATE NEWS CATEGORY
 */

exports.create = function(req, res) {
    const newsCategory = new NewsCategory(req.body);
    newsCategory
        .save()
        .then(data => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: "Save success",
                data
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Create error",
                error
            });
        });
};

/**
 * UPDATE NEWS CATEGORY
 */

exports.update = function(req, res) {
    const id = req.params.id;
    const updateObject = req.body;
    NewsCategory.update({ _id: id }, { $set: updateObject })
        .exec()
        .then(() => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: "Update success"
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Update error. Please check again",
                error
            });
        });
};

/**
 * LOAD NEWS CATEGORY INFO
 */

exports.getInfo = function(req, res, next) {
    NewsCategory.findById(req.params.id)
        .then(news => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: "Get info success",
                data: newsCategory
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Get info error. Please check again",
                error
            });
        });
};

/**
 * REMOVE NEWS
 */

exports.remove = function(req, res, next) {
    const id = req.params.id;
    NewsCategory.findOneAndDelete(id)
        .exec()
        .then(() => {
            return res.status(HttpStatus.OK).json({
                success: true,
                code: HttpStatus.OK,
                message: "Delete success"
            });
        })
        .catch(error => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                success: false,
                code: HttpStatus.BAD_REQUEST,
                message: "Delete error. Please check again",
                error
            });
        });
};
const mongoose = require("mongoose");
const HttpStatus = require("http-status-codes");
const Promise = require("bluebird");

const Schema = mongoose.Schema;

const NewsCategorySchema = new mongoose.Schema({
    name: {
        type: String
    },
    parentID: {
        type: String
    },
    imageURL: {
        type: String
    },
    description: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

NewsCategorySchema.index({ name: 1 });

NewsCategorySchema.statics = {
    get(id) {
        return this.findById(id)
            .exec()
            .then(news => {
                if (news) {
                    return news;
                }
                const err = new APIError("No such news exists!", httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    },
    list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .sort({ createdAt: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec();
    }
};

module.exports = mongoose.model("NewsCategory", NewsCategorySchema);
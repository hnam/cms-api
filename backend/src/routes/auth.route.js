const express = require("express");
const joiValidate = require("../utils/joi.validate");
const authCtrl = require("../modules/auth/controller");
const validator = require("../modules/auth/validator");
const router = express.Router(); // eslint-disable-line new-cap

router.route("/login").post(joiValidate(validator.logIn), authCtrl.logIn);
router
  .route("/change-password")
  .post(joiValidate(validator.changePassword), authCtrl.changePassword);

module.exports = router;

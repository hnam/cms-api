const express = require("express");
const authRoutes = require("./auth.route");
const userRoutes = require("./user.route");
const imageRoutes = require("./image.route");
const newsRoutes = require("./news.route");
const newsCategoryRoutes = require("./newsCategory.route");
const router = express.Router();

/* auth page. */
router.use("/auth", authRoutes);
router.use("/users", userRoutes);
router.use("/image", imageRoutes);
router.use("/news", newsRoutes);
router.use("/news-category", newsCategoryRoutes);
module.exports = router;
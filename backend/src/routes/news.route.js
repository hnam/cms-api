const express = require("express");
const joiValidate = require("../utils/joi.validate");
const newsCtrl = require("../modules/news/controller");
const validator = require("./../modules/news/validator");
const isAuthenticated = require("../middlewares/authenticate");
const router = express.Router(); // eslint-disable-line new-cap

router
    .route("/")
    /** GET /api/news - Get list of news */
    .get(isAuthenticated, newsCtrl.list)
    /** POST /api/news - Create new news */
    .post(isAuthenticated, joiValidate(validator.newsForm), newsCtrl.create);

router
    .route("/:id")

/** GET /api/news/:newsId - Get news */
.get(isAuthenticated, newsCtrl.getInfo)

/** PUT /api/news/:newsId - Update news */
.put(isAuthenticated, joiValidate(validator.newsForm), newsCtrl.update)

/** DELETE /api/news/:newsId - Delete news */
.delete(isAuthenticated, newsCtrl.remove);

module.exports = router;
const express = require("express");
const joiValidate = require("../utils/joi.validate");
const userCtrl = require("../modules/user/controller");
const validator = require("./../modules/user/validator");
const isAuthenticated = require("../middlewares/authenticate");
const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** GET /api/users - Get list of users */
  .get(isAuthenticated, userCtrl.list)
  /** POST /api/users - Create new user */
  .post(isAuthenticated, joiValidate(validator.userForm), userCtrl.create);

router
  .route("/:id")

  /** GET /api/users/:userId - Get user */
  .get(isAuthenticated, userCtrl.getInfo)

  /** PUT /api/users/:userId - Update user */
  .put(isAuthenticated, joiValidate(validator.userForm), userCtrl.update)

  /** DELETE /api/users/:userId - Delete user */
  .delete(isAuthenticated, userCtrl.remove);

module.exports = router;

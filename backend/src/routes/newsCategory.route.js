const express = require("express");
const joiValidate = require("../utils/joi.validate");
const newsCategoryCtrl = require("../modules/newsCategory/controller");
const validator = require("./../modules/newsCategory/validator");
const isAuthenticated = require("../middlewares/authenticate");
const router = express.Router(); // eslint-disable-line new-cap

router
    .route("/")
    /** GET /api/newsCategory - Get list of newsCategory */
    .get(isAuthenticated, newsCategoryCtrl.list)
    /** POST /api/newsCategory - Create new newsCategory */
    .post(
        isAuthenticated,
        joiValidate(validator.newsCategoryForm),
        newsCategoryCtrl.create
    );

router
    .route("/:id")

/** GET /api/newsCategory/:newsCategoryId - Get newsCategory */
.get(isAuthenticated, newsCategoryCtrl.getInfo)

/** PUT /api/newsCategory/:newsCategoryId - Update newsCategory */
.put(
    isAuthenticated,
    joiValidate(validator.newsCategoryForm),
    newsCategoryCtrl.update
)

/** DELETE /api/newsCategory/:newsCategoryId - Delete newsCategory */
.delete(isAuthenticated, newsCategoryCtrl.remove);

module.exports = router;
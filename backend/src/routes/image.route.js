const express = require("express");
const isAuthenticated = require("../middlewares/authenticate");
const router = express.Router(); // eslint-disable-line new-cap
const cloudStorageCtrl = require("../modules/image/cloud-storage.controller");

router
    .route("/upload")
    /** POST /api/images/upload - UPLOAD IMAGE */
    .post(isAuthenticated, cloudStorageCtrl.upload);
router
    .route("/upload-multi")
    /** POST /api/images/upload-multi - UPLOAD MULTI IMAGE */
    .post(isAuthenticated, cloudStorageCtrl.uploadMulti);

module.exports = router;
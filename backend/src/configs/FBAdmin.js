const admin = require("firebase-admin");
const serviceAccount = require("../../cmsproject-152f3-firebase-adminsdk-tp7xc-e63e8bd138.json");
const config = require("./FBConfig");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: config.storageBucket
});
const bucket = admin.storage().bucket();
module.exports = { admin, bucket };

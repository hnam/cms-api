const { admin } = require("../configs/FBAdmin");

const HttpStatus = require("http-status-codes");
module.exports = (req, res, next) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer ")
  ) {
    idToken = req.headers.authorization.split("Bearer ")[1];
  } else {
    return res
      .status(HttpStatus.UNAUTHORIZED)
      .json({ error: "You are not authorized to perform this operation!" });
  }

  admin
    .auth()
    .verifyIdToken(idToken)
    .then(decodedToken => {
      if (decodedToken.uid) return next();
      return res.status(HttpStatus.UNAUTHORIZED).json({
        error: "Error while verifying token"
      });
    })
    .catch(error => {
      return res.status(HttpStatus.FORBIDDEN).json({
        error: "Error while verifying token"
      });
    });
};

const HttpStatus = require('http-status-codes');

module.exports = (err, req, res, next) => {
  if (err.isJoi) {
    const error = {
      code: HttpStatus.BAD_REQUEST,
      message: HttpStatus.getStatusText(HttpStatus.BAD_REQUEST),
      details: err.details && err.details.map(err => {
        return {
          message: err.message,
          param: err.path
        };
      })
    };
    return res.status(HttpStatus.BAD_REQUEST).json(error);
  }
  return next(err);
}